(function ($) {
  /* navbar */
  
  $(document).ready(function() {
    // run test on initial page load
    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize);
  });
  
  $('.nav-list').click( 
    function(){
      if ($(this).find('div.drop-menu').hasClass('usa-sr-only'))
        {showDrop(this)}
      else
        {hideDrop(this)}
    }
  )
    
  function checkSize(){
    console.log($('nav#navbar').css('flex-flow'));
    if ($('nav#navbar').css('flex-flow') == 'row wrap'){
      $('.nav-list').hover(function(){
        showDrop(this);
        setDropCSS(this);
      },function(){hideDrop(this)});
    } else {
      $('.nav-list').unbind('mouseenter mouseleave');
    }
  }  
  
  function hideDrop(dropMenuParent) {
    $(dropMenuParent).find('div.drop-menu').addClass('usa-sr-only').removeClass('showDrop');
  }
  
  function showDrop(dropMenuParent) {
    $(dropMenuParent).find('div.drop-menu').removeClass('usa-sr-only').addClass('showDrop');
  }
  
  function setDropCSS(dropMenuParent) {
    var position = $('nav#navbar').position();
    var positionTop = position.top;
    var navMarginLeft = $('nav#navbar').css("margin-left");
    var positionLeft = parseInt(navMarginLeft) + 15.5;
    positionLeft = positionLeft.toString() + "px";
    var navHeight = $('nav#navbar').outerHeight();
    var navWidth = parseInt($('nav#navbar').outerWidth());
    navWidth = navWidth.toString() + "px";
    var dropPostionTop = positionTop + navHeight;
    $(dropMenuParent).find('div.drop-menu').css({"top": dropPostionTop, "width": navWidth});
  }
  
  
  /* Right Nav open/close 
  $('.layout__region--third h2').append('<i class="fas fa-angle-down"></i>');
  
  $('.layout__region--third div.contextual-region.view').addClass('usa-sr-only');
  
  $('.layout__region--third h2').on( 'click', function() {
    if ($( this ).find( 'svg' ).hasClass('fa-angle-down')) {
      $( this ).find( 'svg' ).addClass('fa-angle-up').removeClass('fa-angle-down');
    } else {
      $( this ).find( 'svg' ).addClass('fa-angle-down').removeClass('fa-angle-up ');
    }
    $( this ).closest( '.views-element-container' ).find( 'div.contextual-region.view' ).toggleClass('usa-sr-only');
  })*/
  
  
  /* header search */
  $('.usa-search.usa-search-small [type=submit]').val('');
  $('.usa-search.usa-search-small [type=text]').val('');
    
    
})(jQuery);