#!/bin/bash

function isinstalled {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

packages=(httpd php71w php71w-common php71w-gd php71w-pear php71w-mbstring php71w-xml php71w-pgsql php71w-opcache postgresql96 policycoreutils-python)

# Add PHP 7 repos
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# Update repos and install utilities apps
yum update -y
yum install wget git vim -y
yum install  https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm -y

# Check if packges are installed
for var in "${packages[@]}"
do
  if isinstalled ${var}; then
    echo "${var} is installed"
  else
    echo "${var} is not installed, installing ${var}"
    yum install ${var} -y
  fi
done

#Configure Apache
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.`date +%Y%m%d_%H%M`
sed -i ':a;N;$!ba;s/AllowOverride None/AllowOverride All/2' /etc/httpd/conf/httpd.conf 

# Start and enable Apache
if $(systemctl is-active --quiet httpd); then
   echo "Apache service is running..."
else
  echo "Apache is not running, starting Apache..."
  systemctl start httpd
  systemctl enable httpd
fi

# Install Composer and Drush
cd /tmp
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
COMPOSER_HOME=/opt/drush COMPOSER_BIN_DIR=/usr/local/bin COMPOSER_VENDOR_DIR=/opt/drush /usr/local/bin/composer global require drush/drush:9.1.0
composer --version
drush version

# Download repo and extract to web root folder
#wget -O /tmp/qtso-web.tgz http://ec2-54-91-219-192.compute-1.amazonaws.com:8081/repository/NexusArtifactUploader/dev/qtso-test/0.0.1/qtso-test-0.0.1.tgz
#tar xvf /tmp/qtso-web.tgz -C /tmp
#rm -fr /var/www/html/* /tmp/qtso-test/vendor/
#mv /tmp/qtso-test/{.,}* /var/www/html
#rm -fr /tmp/qtso-test /tmp/qtso-web.tgz
# =======
# wget -O /tmp/qtso-web.tgz http://ec2-54-91-219-192.compute-1.amazonaws.com:8081/repository/NexusArtifactUploader/dev/qtso-test/0.0.1/qtso-test-0.0.1.tgz
# tar xvf /tmp/qtso-web.tgz -C /tmp
# rm -fr /var/www/html/* /tmp/qtso-test/vendor/
# mv /tmp/qtso-test/{.,}* /var/www/html
# rm -fr /tmp/qtso-test /tmp/qtso-web.tgz
# >>>>>>> a99134411d16d7af84788693ecba76e14acd336c

# Install Drupal dependencies
#cd /var/www/html
# /usr/local/bin/composer install

# Set permissions on web folder and selinux
#chown -R apache:apache /var/www/html/
#chcon -R -t httpd_sys_content_rw_t /var/www/html
#setenforce 0
#setsebool -P httpd_can_network_connect_db 1
#semanage port -a -t http_port_t -p tcp 8080
#setenforce 1

# Restart apache
systemctl restart httpd

drush status

# End of file
